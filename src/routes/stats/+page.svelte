<script lang="ts">
	import { onMount } from 'svelte';
	import { pb } from '$lib/pocketbase';
	import { parse } from 'svelte/compiler';
	import { convertMinutes } from '$lib/utils';

	type Action = {
		type: string;
		timestamp: string;
	};

	let stats = {
		avgSleepPerNap: 0,
		avgNapsPerDay: 0,
		avgSleepPerDay: 0,
		avgDiaperChanges: 0,
		avgPoops: 0,
		avgEatingTimes: 0
	};

	onMount(async () => {
		const actions = await fetchAllActions();
		calculateStats(actions);
	});

	async function fetchAllActions() {
		// Fetch all actions. Adjust as needed for date range or pagination
		const result = await pb.collection('actions').getList(1, 1000, {
			sort: 'timestamp'
		});
		return result.items;
	}

	function calculateStats(actions: Action[]) {
		// Implement the logic to calculate each statistic
		// This is a placeholder and should be replaced with actual calculation logic
		stats.avgSleepPerNap = calculateAvgSleepPerNap(actions);
		stats.avgNapsPerDay = calculateAvgNapsPerDay(actions);
		stats.avgSleepPerDay = calculateAvgSleepPerDay(actions);
		stats.avgDiaperChanges = calculateAvgDiaperChanges(actions);
		stats.avgPoops = calculateAvgDaysBetweenPoops(actions);
		stats.avgEatingTimes = calculateAvgMealsPerDay(actions);
	}

	function calculateAvgSleepPerNap(actions: Action[]): number {
		const sleepActions = actions.filter((a) => a.type === 'asleep' || a.type === 'awake');
		sleepActions.sort((a, b) => new Date(a.timestamp).getTime() - new Date(b.timestamp).getTime());

		let totalSleepTime = 0;
		let napCount = 0;

		for (let i = 0; i < sleepActions.length - 1; i++) {
			if (sleepActions[i].type === 'asleep' && sleepActions[i + 1].type === 'awake') {
				totalSleepTime +=
					new Date(sleepActions[i + 1].timestamp).getTime() -
					new Date(sleepActions[i].timestamp).getTime();
				napCount++;
			}
		}

		return Math.round(napCount > 0 ? totalSleepTime / napCount / 1000 / 60 : 0); // returns average time in minutes
	}

	function calculateAvgNapsPerDay(actions: Action[]): number {
		const sortedActions = actions.sort(
			(a, b) => new Date(a.timestamp).getTime() - new Date(b.timestamp).getTime()
		);
		const daysMap = new Map<string, number>();

		for (let i = 0; i < sortedActions.length - 1; i++) {
			if (sortedActions[i].type === 'asleep' && sortedActions[i + 1].type === 'awake') {
				const asleepDate = new Date(sortedActions[i].timestamp);
				const awakeDate = new Date(sortedActions[i + 1].timestamp);

				if (asleepDate.toDateString() === awakeDate.toDateString()) {
					const dayKey = asleepDate.toDateString();
					daysMap.set(dayKey, (daysMap.get(dayKey) || 0) + 1);
				}
			}
		}

		const totalNaps = Array.from(daysMap.values()).reduce((acc, naps) => acc + naps, 0);
		return daysMap.size > 0 ? parseFloat((totalNaps / daysMap.size).toFixed(1)) : 0;
	}

	function calculateAvgSleepPerDay(actions: Action[]): number {
		const sortedActions = actions.sort(
			(a, b) => new Date(a.timestamp).getTime() - new Date(b.timestamp).getTime()
		);
		const sleepDurations: number[] = [];

		for (let i = 0; i < sortedActions.length - 1; i++) {
			if (sortedActions[i].type === 'asleep' && sortedActions[i + 1].type === 'awake') {
				const asleepDate = new Date(sortedActions[i].timestamp);
				const awakeDate = new Date(sortedActions[i + 1].timestamp);

				if (asleepDate.toDateString() === awakeDate.toDateString()) {
					const sleepDuration = (awakeDate.getTime() - asleepDate.getTime()) / 1000 / 60; // Sleep duration in minutes
					sleepDurations.push(sleepDuration);
				}
			}
		}

		if (sleepDurations.length === 0) return 0;

		const totalSleepDuration = sleepDurations.reduce((a, b) => a + b, 0);
		return Math.round(totalSleepDuration / sleepDurations.length);
	}

	function calculateAvgDiaperChanges(actions: any[]): number {
		const today = new Date().toDateString(); // Get today's date as a string

		// Filter actions by type "food" and exclude today's actions
		const foodActions = actions.filter((action) => {
			const actionDate = new Date(action.timestamp).toDateString(); // Convert timestamp to date string
			return action.type === 'diaper' && actionDate !== today;
		});

		// Group by date
		const actionsByDate: { [key: string]: number } = {};
		foodActions.forEach((action) => {
			const date = new Date(action.timestamp).toDateString(); // Convert timestamp to date string
			if (!actionsByDate[date]) {
				actionsByDate[date] = 1;
			} else {
				actionsByDate[date]++;
			}
		});

		// Calculate total actions and number of days
		const totalActions = foodActions.length;
		const numberOfDays = Object.keys(actionsByDate).length;

		// Calculate average actions per day, excluding today
		const average = numberOfDays > 0 ? totalActions / numberOfDays : 0;

		// Return the average to one decimal place
		return parseFloat(average.toFixed(1));
	}

	function calculateAvgDaysBetweenPoops(actions: Action[]): number {
		let poopDates = [];

		// Extract dates of 'poop' actions
		for (const action of actions) {
			if (action.type === 'poop') {
				const actionDate = new Date(action.timestamp.split('T')[0]); // Extract the date part
				poopDates.push(actionDate);
			}
		}

		// Sort dates
		poopDates.sort((a, b) => a.getTime() - b.getTime());

		if (poopDates.length < 2) {
			return 0; // Not enough data to calculate intervals
		}

		let totalDays = 0;
		for (let i = 1; i < poopDates.length; i++) {
			const interval =
				(poopDates[i].getTime() - poopDates[i - 1].getTime()) / (1000 * 60 * 60 * 24); // Convert to days
			totalDays += interval;
		}

		return parseFloat((totalDays / (poopDates.length - 1)).toFixed(1));
	}

	function calculateAvgMealsPerDay(actions: any[]): number {
		const today = new Date().toDateString(); // Get today's date as a string

		// Filter actions by type "food" and exclude today's actions
		const foodActions = actions.filter((action) => {
			const actionDate = new Date(action.timestamp).toDateString(); // Convert timestamp to date string
			return action.type === 'food' && actionDate !== today;
		});

		// Group by date
		const actionsByDate: { [key: string]: number } = {};
		foodActions.forEach((action) => {
			const date = new Date(action.timestamp).toDateString(); // Convert timestamp to date string
			if (!actionsByDate[date]) {
				actionsByDate[date] = 1;
			} else {
				actionsByDate[date]++;
			}
		});

		// Calculate total actions and number of days
		const totalActions = foodActions.length;
		const numberOfDays = Object.keys(actionsByDate).length;

		// Calculate average actions per day, excluding today
		const average = numberOfDays > 0 ? totalActions / numberOfDays : 0;

		// Return the average to one decimal place
		return parseFloat(average.toFixed(1));
	}

	function calculateAgeFormatted(birthdate: Date): string {
		const now = new Date();
		const birthDate = new Date(birthdate);
		const diffInMilliseconds = now.getTime() - birthDate.getTime();
		const diffInDays = diffInMilliseconds / (1000 * 3600 * 24);
		const diffInWeeks = diffInDays / 7;
		const diffInMonths = diffInDays / 30.4375; // Average days per month in a year
		const diffInYears = diffInDays / 365.25; // Average, accounting for leap years

		// Determine the largest unit (years, months, weeks, days) for the age
		if (diffInYears >= 1) {
			return formatAge(diffInYears, 'year');
		} else if (diffInMonths >= 1) {
			return formatAge(diffInMonths, 'month');
		} else if (diffInWeeks >= 1) {
			return formatAge(diffInWeeks, 'week');
		} else {
			return formatAge(diffInDays, 'day');
		}
	}

	function formatAge(value: number, unit: string): string {
		// Round to nearest quarter
		const nearestQuarter = Math.round(value * 4) / 4;
		const integerPart = Math.floor(nearestQuarter);
		const fractionPart = nearestQuarter - integerPart;
		let fractionString = '';

		switch (fractionPart) {
			case 0.25:
				fractionString = '¼';
				break;
			case 0.5:
				fractionString = '½';
				break;
			case 0.75:
				fractionString = '¾';
				break;
			case 0:
				fractionString = '';
				break;
			default:
				// This case should not happen with rounding to nearest quarter
				break;
		}

		const ageString = `${integerPart}${fractionString} ${unit}${integerPart > 1 || integerPart === 0 ? 's' : ''}`;
		return ageString;
	}
</script>

<section>
	<h2>Stats</h2>
	<ul>
		<li>{calculateAgeFormatted(new Date('2023-08-30'))} old</li>
	</ul>
	<h3>Input</h3>
	<ul>
		<li>{stats.avgEatingTimes} meals / day</li>
	</ul>
	<h3>Output</h3>
	<ul>
		<li>{stats.avgDiaperChanges} diapers / day</li>
		<li>{stats.avgPoops} days between poops</li>
	</ul>
	<h3>Downtime</h3>
	<ul>
		<li>{convertMinutes(stats.avgSleepPerNap)} / nap</li>
		<li>{stats.avgNapsPerDay} naps / day</li>
		<!-- <li>{stats.avgSleepPerDay} min of sleep / day</li> -->
		<li>{convertMinutes(Math.round(stats.avgSleepPerNap * stats.avgNapsPerDay))} of sleep / day</li>
	</ul>
</section>
