import { writable } from 'svelte/store';

interface UserRecord {
    username: string;
    email: string;
    password: string;
}

interface User {
    record: UserRecord;
}

export const user = writable<User>();